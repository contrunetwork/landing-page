import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/style.scss";
import "./assets/media.scss";
import "./assets/buttons.scss";
import "./assets/utils.scss";
import "./assets/forms.scss";
import "@fortawesome/fontawesome-free/css/all.css";
import axios from "axios";
import Toastify from "toastify-js";
var VueCookie = require("vue-cookie");
import VuePageTransition from "vue-page-transition";

Vue.use(VueCookie);
Vue.use(VuePageTransition);

Vue.filter("toCurrency", function(value) {
  if (typeof value !== "number") {
    return value;
  }
  var formatter = new Intl.NumberFormat("en-US", {
    style: "decimal",

    minimumFractionDigits: 0,
  });
  return formatter.format(value);
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  axios,
  Toastify,
  render: (h) => h(App),
}).$mount("#app");
