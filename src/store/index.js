import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    environment: 1, // 0 = development 1 = production,
    mobileMenu: false,
    loginPopup: false,
    registerPopup: false,
    baseurl: "https://backendcd.singlecode.org/api",
    url: "http://localhost:8080",
    cart: [],
    tokenAccess: null,
    username: "",
    role: "",
    logo: null,
    authStatusRequest: true,
    userPopover: false,
    cartPopover: false,
  },
  mutations: {
    setToken(state, tokenAccess) {
      state.tokenAccess = tokenAccess;
    },
    setUser(state, username) {
      state.username = username;
    },
    setRole(state, role) {
      state.role = role;
    },
    setId(state, uid) {
      state.uid = uid;
    },
    setLogo(state, logo) {
      state.logo = logo;
    },
    setAuthStatusRequest(state, authStatusRequest) {
      state.authStatusRequest = authStatusRequest;
    },
    changeLoginPopup(state, loginPopup) {
      state.loginPopup = loginPopup;
    },
    changeRegisterPopup(state, registerPopup) {
      state.registerPopup = registerPopup;
    },
    changeMenuMobile(state, mobileMenu) {
      state.mobileMenu = mobileMenu;
    },
    setUserPopover(state, userPopover) {
      state.userPopover = userPopover;
    },
    setCartPopover(state, cartPopover) {
      state.cartPopover = cartPopover;
    },
    setCart(state, cart) {
      state.cart = cart;
    },
    addToCart(state, product) {
      var newCart = [];
      console.log("NEW PRODUCT IN CART: ", product);
      var _new = true;

      if (state.cart) {
        state.cart.forEach((item) => {
          if (item.id == product.id) {
            item.quantity++;
            _new = false;
          }

          newCart.push(item);
        });
      } else {
        state.cart = [];
      }

      if (state.cart.length == 0 || _new == true) {
        newCart.push({
          quantity: 1,
          name: product.name,
          id: product.id,
          feature: product.featureImageURL,
          price: product.price,
          supplierID: product.supplierID,
        });
      }

      console.log("NEW CART: ", newCart);
      state.cart = newCart;
    },
    deleteOneToCart(state, product) {
      var newCart = [];
      state.cart.forEach((item) => {
        if (item.id == product.id) {
          item.quantity--;
        }
        if (item.quantity > 0) {
          newCart.push(item);
        }
      });
      state.cart = newCart;
    },
    deleteAllToCart(state, product) {
      var newCart = [];
      state.cart.forEach((item) => {
        if (item.id != product.id) {
          newCart.push(item);
        }
      });
      state.cart = newCart;
    },
  },
  actions: {},
  modules: {},
  getters: {
    loginPopup: (state) => state.loginPopup,
    registerPopup: (state) => state.registerPopup,
    mobileMenu: (state) => state.mobileMenu,
    baseurl: (state) => state.baseurl,
    environment: (state) => state.environment,
    url: (state) => state.url,
    cart: (state) => state.cart,
    tokenAccess: (state) => state.tokenAccess,
    authStatusRequest: (state) => state.authStatusRequest,
    role: (state) => state.role,
    uid: (state) => state.uid,
    username: (state) => state.username,
    userPopover: (state) => state.userPopover,
    cartPopover: (state) => state.cartPopover,
    logo: (state) => state.logo,
  },
});
