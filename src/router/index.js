import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import PreRegister from "../views/PreRegister.vue";
import Shop from "../views/Shop.vue";
import Suppliers from "../views/Suppliers.vue";
import Installers from "../views/Installers.vue";
import Events from "../views/Events.vue";
import About from "../views/About.vue";
import SingleSupplier from "../views/SingleSupplier.vue";
import NotFound from "../views/NotFound.vue";
import Cart from "../views/Cart.vue";
import Checkout from "../views/Checkout.vue";
import Login from "../views/Login.vue";
import SingleOrder from "../views/SingleOrder.vue";
import Logout from "../views/Logout.vue";
import regSupplier from "../views/regSupplier.vue";
import regClient from "../views/regClient.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/register",
    name: "PreRegister",
    component: PreRegister,
  },
  {
    path: "/club",
    name: "Shop",
    component: Shop,
    props: true,
  },
  {
    path: "/suplidores",
    name: "Suppliers",
    component: Suppliers,
    props: true,
  },
  {
    path: "/instaladores",
    name: "Installers",
    component: Installers,
  },
  {
    path: "/eventos",
    name: "Events",
    component: Events,
  },
  {
    path: "/nosotros",
    name: "About",
    component: About,
  },
  {
    path: "/carrito",
    name: "Cart",
    component: Cart,
  },
  {
    path: "/pago",
    name: "Checkout",
    component: Checkout,
  },
  {
    path: "/entrar",
    name: "Login",
    component: Login,
  },
  {
    path: "/registro/suplidor",
    name: "regSupplier",
    component: regSupplier,
  },
  {
    path: "/registro/cliente",
    name: "regClient",
    component: regClient,
  },
  {
    path: "/logout",
    name: "Logout",
    component: Logout,
  },
  {
    path: "/suplidor/:id",
    name: "SingleSupplier",
    component: SingleSupplier,
    props: true,
  },
  {
    path: "/order/:id",
    name: "SingleOrder",
    component: SingleOrder,
    props: true,
  },  
  {
    // matches everything else
    path: "*",
    name: "NotFound",
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
